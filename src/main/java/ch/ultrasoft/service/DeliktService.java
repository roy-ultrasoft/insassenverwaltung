package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.DeliktEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class DeliktService extends BaseService<DeliktEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public DeliktService(){
        super(DeliktEntity.class);
    }
    
    @Transactional
    public List<DeliktEntity> findAllDeliktEntities() {
        
        return entityManager.createQuery("SELECT o FROM Delikt o ", DeliktEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Delikt o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(DeliktEntity delikt) {

        /* This is called before a Delikt is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<DeliktEntity> findAvailableDelikts(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Delikt o WHERE o.aufenthalt.id = :idAufenthalt", DeliktEntity.class).setParameter("idAufenthalt", aufenthalt.getId()).getResultList();
    }

    @Transactional
    public List<DeliktEntity> findDeliktsByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Delikt o WHERE o.aufenthalt = :aufenthalt", DeliktEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

}
