package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.BelegungEntity;
import ch.ultrasoft.domain.DeliktEntity;
import ch.ultrasoft.domain.EffekteEntity;
import ch.ultrasoft.domain.InsasseEntity;
import ch.ultrasoft.domain.JournalEntity;
import ch.ultrasoft.domain.KonsultationEntity;
import ch.ultrasoft.domain.PersonEntity;
import ch.ultrasoft.domain.StrafeEntity;
import ch.ultrasoft.domain.UrteilEntity;
import ch.ultrasoft.domain.ZelleEntity;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class AufenthaltService extends BaseService<AufenthaltEntity> implements Serializable {

	private static final long serialVersionUID = 1L;

	public AufenthaltService() {
		super(AufenthaltEntity.class);
	}

	@Transactional
	public List<AufenthaltEntity> findAllAufenthaltEntities() {

		return entityManager.createQuery("SELECT o FROM Aufenthalt o ", AufenthaltEntity.class).getResultList();
	}

	@Override
	@Transactional
	public long countAllEntries() {
		return entityManager.createQuery("SELECT COUNT(o) FROM Aufenthalt o", Long.class).getSingleResult();
	}

	@Override
	protected void handleDependenciesBeforeDelete(AufenthaltEntity aufenthalt) {

		/*
		 * This is called before a Aufenthalt is deleted. Place here all the steps to
		 * cut dependencies to other entities
		 */

		this.cutAllAufenthaltBelegungsAssignments(aufenthalt);

		this.cutAllAufenthaltDeliktsAssignments(aufenthalt);

		this.cutAllAufenthaltEffektesAssignments(aufenthalt);

		this.cutAllAufenthaltUrteilsAssignments(aufenthalt);

		this.cutAllAufenthaltJournalsAssignments(aufenthalt);

		this.cutAllAufenthaltKonsultationsAssignments(aufenthalt);

	}

	// Remove all assignments from all belegung a aufenthalt. Called before delete a
	// aufenthalt.
	@Transactional
	private void cutAllAufenthaltBelegungsAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Belegung c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	// Remove all assignments from all delikt a aufenthalt. Called before delete a
	// aufenthalt.
	@Transactional
	private void cutAllAufenthaltDeliktsAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Delikt c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	// Remove all assignments from all effekte a aufenthalt. Called before delete a
	// aufenthalt.
	@Transactional
	private void cutAllAufenthaltEffektesAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Effekte c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	// Remove all assignments from all urteil a aufenthalt. Called before delete a
	// aufenthalt.
	@Transactional
	private void cutAllAufenthaltUrteilsAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Urteil c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	// Remove all assignments from all journal a aufenthalt. Called before delete a
	// aufenthalt.
	@Transactional
	private void cutAllAufenthaltJournalsAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Journal c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	// Remove all assignments from all konsultation a aufenthalt. Called before
	// delete a aufenthalt.
	@Transactional
	private void cutAllAufenthaltKonsultationsAssignments(AufenthaltEntity aufenthalt) {
		entityManager.createQuery("UPDATE Konsultation c SET c.aufenthalt = NULL WHERE c.aufenthalt = :p")
				.setParameter("p", aufenthalt).executeUpdate();
	}

	@Transactional
	public void prepare() {
		System.out.println("Start");
		for (int i = 0; i < 20; i++) {

			System.out.println("Person: " + i);
			PersonEntity person = new PersonEntity();
			person.setName(randomString());
			person.setVorname(randomString());
			person.setGeburtsdatum(new Date());
			person = (PersonEntity) save(person);
			System.out.println("Insasse");
			InsasseEntity insasse = new InsasseEntity();
			insasse.setPerson(person);
			insasse = (InsasseEntity) save(insasse);
			System.out.println("Aufenthalt");
			AufenthaltEntity aufenthalt = new AufenthaltEntity();
			aufenthalt.setVon(new Date());
			aufenthalt.setInsasse(insasse);
			aufenthalt = save(aufenthalt);
			System.out.println("Zelle");
			ZelleEntity zelle = new ZelleEntity();
			zelle.setText("Zelle " + i);
			zelle = (ZelleEntity) save(zelle);

			int random = getRandomInt(12);
			System.out.println("Belegung: " + random);
			for (int k = 0; k < random; k++) {
				BelegungEntity belegung = new BelegungEntity();
				belegung.setVon(new Date());
				belegung.setZelle(zelle);
				if (k != random - 1)
					belegung.setBis(new Date());
				belegung.setAufenthalt(aufenthalt);
				belegung = (BelegungEntity) save(belegung);
			}

			random = getRandomInt(6);
			System.out.println("Delikt: " + random);
			for (int k = 0; k < random; k++) {
				DeliktEntity delikt = new DeliktEntity();
				delikt.setText(randomString());
				delikt.setAufenthalt(aufenthalt);
				delikt = (DeliktEntity) save(delikt);
			}

			random = getRandomInt(4);
			System.out.println("Urteil: " + random);
			for (int k = 0; k < random; k++) {
				UrteilEntity urteil = new UrteilEntity();
				urteil.setDatum(new Date());
				urteil.setText(randomString());
				urteil.setAufenthalt(aufenthalt);
				urteil = (UrteilEntity) save(urteil);
				random = getRandomInt(3);
				for (int j = 0; j < random; j++) {
					StrafeEntity strafe = new StrafeEntity();
					strafe.setAddieren(true);
					strafe.setJahre(1);
					strafe.setTage(3);
					strafe.setUrteil(urteil);
					strafe.setText(randomString());
					strafe = (StrafeEntity) save(strafe);
				}
			}

			random = getRandomInt(20);
			System.out.println("Journal: " + random);
			for (int k = 0; k < random; k++) {
				JournalEntity journal = new JournalEntity();
				journal.setAufenthalt(aufenthalt);
				journal.setDatum(new Date());
				journal.setText(String.format("%s\n%s\n%s\n%s\n%s\n", randomString(), randomString(), randomString(),
						randomString(), randomString()));
				journal = (JournalEntity) save(journal);
			}

			random = getRandomInt(10);
			System.out.println("Konsultation: " + random);
			for (int k = 0; k < random; k++) {
				KonsultationEntity konsultation = new KonsultationEntity();
				konsultation.setAufenthalt(aufenthalt);
				konsultation.setVon(new Date());
				konsultation.setBis(new Date());
				konsultation.setText(String.format("%s\n%s\n%s\n%s\n%s\n", randomString(), randomString(),
						randomString(), randomString(), randomString()));
				konsultation = (KonsultationEntity) save(konsultation);
			}

			random = getRandomInt(6);
			System.out.println("Effekten: " + random);
			for (int k = 0; k < random; k++) {
				EffekteEntity effekte = new EffekteEntity();
				effekte.setAnzahl(getRandomInt(10));
				effekte.setText(randomString());
				effekte.setAufenthalt(aufenthalt);
				effekte = (EffekteEntity) save(effekte);
			}
		}
		System.out.println("End");
	}

	private int getRandomInt(int max) {
		return (int) ((Math.random() * 1000 * max) % max - 1) + 1;
	}

	private String randomString() {
		return new BigInteger(20, new SecureRandom()).toString(32);
	}
}
