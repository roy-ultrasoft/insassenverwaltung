package ch.ultrasoft.service;

import ch.ultrasoft.domain.InsasseEntity;
import ch.ultrasoft.domain.PersonEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class InsasseService extends BaseService<InsasseEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public InsasseService(){
        super(InsasseEntity.class);
    }
    
    @Transactional
    public List<InsasseEntity> findAllInsasseEntities() {
        
        return entityManager.createQuery("SELECT o FROM Insasse o ", InsasseEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Insasse o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(InsasseEntity insasse) {

        /* This is called before a Insasse is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<InsasseEntity> findAvailableInsasses(PersonEntity person) {
        return entityManager.createQuery("SELECT o FROM Insasse o WHERE o.person IS NULL", InsasseEntity.class).getResultList();
    }

    @Transactional
    public List<InsasseEntity> findInsassesByPerson(PersonEntity person) {
        return entityManager.createQuery("SELECT o FROM Insasse o WHERE o.person = :person", InsasseEntity.class).setParameter("person", person).getResultList();
    }

}
