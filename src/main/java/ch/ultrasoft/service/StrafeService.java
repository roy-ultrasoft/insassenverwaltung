package ch.ultrasoft.service;

import ch.ultrasoft.domain.StrafeEntity;
import ch.ultrasoft.domain.UrteilEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class StrafeService extends BaseService<StrafeEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public StrafeService(){
        super(StrafeEntity.class);
    }
    
    @Transactional
    public List<StrafeEntity> findAllStrafeEntities() {
        
        return entityManager.createQuery("SELECT o FROM Strafe o ", StrafeEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Strafe o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(StrafeEntity strafe) {

        /* This is called before a Strafe is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<StrafeEntity> findAvailableStrafes(UrteilEntity urteil) {
        return entityManager.createQuery("SELECT o FROM Strafe o WHERE o.urteil IS NULL", StrafeEntity.class).getResultList();
    }

    @Transactional
    public List<StrafeEntity> findStrafesByUrteil(UrteilEntity urteil) {
        return entityManager.createQuery("SELECT o FROM Strafe o WHERE o.urteil = :urteil", StrafeEntity.class).setParameter("urteil", urteil).getResultList();
    }

}
