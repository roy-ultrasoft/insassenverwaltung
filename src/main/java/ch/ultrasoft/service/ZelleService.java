package ch.ultrasoft.service;

import ch.ultrasoft.domain.ZelleEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class ZelleService extends BaseService<ZelleEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public ZelleService(){
        super(ZelleEntity.class);
    }
    
    @Transactional
    public List<ZelleEntity> findAllZelleEntities() {
        
        return entityManager.createQuery("SELECT o FROM Zelle o ", ZelleEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Zelle o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(ZelleEntity zelle) {

        /* This is called before a Zelle is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllZelleBelegungsAssignments(zelle);
        
    }

    // Remove all assignments from all belegung a zelle. Called before delete a zelle.
    @Transactional
    private void cutAllZelleBelegungsAssignments(ZelleEntity zelle) {
        entityManager
                .createQuery("UPDATE Belegung c SET c.zelle = NULL WHERE c.zelle = :p")
                .setParameter("p", zelle).executeUpdate();
    }
    
}
