package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.KonsultationEntity;
import ch.ultrasoft.domain.PersonEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class KonsultationService extends BaseService<KonsultationEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public KonsultationService(){
        super(KonsultationEntity.class);
    }
    
    @Transactional
    public List<KonsultationEntity> findAllKonsultationEntities() {
        
        return entityManager.createQuery("SELECT o FROM Konsultation o ", KonsultationEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Konsultation o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(KonsultationEntity konsultation) {

        /* This is called before a Konsultation is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<KonsultationEntity> findAvailableKonsultations(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Konsultation o WHERE o.aufenthalt IS NULL", KonsultationEntity.class).getResultList();
    }

    @Transactional
    public List<KonsultationEntity> findKonsultationsByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Konsultation o WHERE o.aufenthalt = :aufenthalt", KonsultationEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

    @Transactional
    public List<KonsultationEntity> findAvailableKonsultations(PersonEntity person) {
        return entityManager.createQuery("SELECT o FROM Konsultation o where o.id not in (select o.id from Konsultation o join o.personens p where p = :p)", KonsultationEntity.class).setParameter("p", person).getResultList();
    }

    @Transactional
    public List<KonsultationEntity> findKonsultationsByPersonen(PersonEntity person) {
        return entityManager.createQuery("SELECT o FROM Konsultation o where o.id in (select o.id from Konsultation o join o.personens p where p = :p)", KonsultationEntity.class).setParameter("p", person).getResultList();
    }

    @Transactional
    public KonsultationEntity fetchPersonens(KonsultationEntity konsultation) {
        konsultation = find(konsultation.getId());
        konsultation.getPersonens().size();
        return konsultation;
    }
    
}
