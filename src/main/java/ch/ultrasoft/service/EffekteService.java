package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.EffekteEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class EffekteService extends BaseService<EffekteEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public EffekteService(){
        super(EffekteEntity.class);
    }
    
    @Transactional
    public List<EffekteEntity> findAllEffekteEntities() {
        
        return entityManager.createQuery("SELECT o FROM Effekte o ", EffekteEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Effekte o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(EffekteEntity effekte) {

        /* This is called before a Effekte is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<EffekteEntity> findAvailableEffektes(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Effekte o WHERE o.aufenthalt IS NULL", EffekteEntity.class).getResultList();
    }

    @Transactional
    public List<EffekteEntity> findEffektesByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Effekte o WHERE o.aufenthalt = :aufenthalt", EffekteEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

}
