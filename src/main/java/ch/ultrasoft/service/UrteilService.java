package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.UrteilEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class UrteilService extends BaseService<UrteilEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public UrteilService(){
        super(UrteilEntity.class);
    }
    
    @Transactional
    public List<UrteilEntity> findAllUrteilEntities() {
        
        return entityManager.createQuery("SELECT o FROM Urteil o ", UrteilEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Urteil o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(UrteilEntity urteil) {

        /* This is called before a Urteil is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllUrteilStrafesAssignments(urteil);
        
    }

    // Remove all assignments from all strafe a urteil. Called before delete a urteil.
    @Transactional
    private void cutAllUrteilStrafesAssignments(UrteilEntity urteil) {
        entityManager
                .createQuery("UPDATE Strafe c SET c.urteil = NULL WHERE c.urteil = :p")
                .setParameter("p", urteil).executeUpdate();
    }
    
    @Transactional
    public List<UrteilEntity> findAvailableUrteils(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Urteil o WHERE o.aufenthalt IS NULL", UrteilEntity.class).getResultList();
    }

    @Transactional
    public List<UrteilEntity> findUrteilsByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Urteil o WHERE o.aufenthalt = :aufenthalt", UrteilEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

}
