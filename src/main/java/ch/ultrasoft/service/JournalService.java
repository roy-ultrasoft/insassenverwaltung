package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.JournalEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class JournalService extends BaseService<JournalEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public JournalService(){
        super(JournalEntity.class);
    }
    
    @Transactional
    public List<JournalEntity> findAllJournalEntities() {
        
        return entityManager.createQuery("SELECT o FROM Journal o ", JournalEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Journal o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(JournalEntity journal) {

        /* This is called before a Journal is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<JournalEntity> findAvailableJournals(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Journal o WHERE o.aufenthalt IS NULL", JournalEntity.class).getResultList();
    }

    @Transactional
    public List<JournalEntity> findJournalsByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Journal o WHERE o.aufenthalt = :aufenthalt", JournalEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

}
