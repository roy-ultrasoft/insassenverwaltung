package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.BelegungEntity;
import ch.ultrasoft.domain.DeliktEntity;
import ch.ultrasoft.domain.EffekteEntity;
import ch.ultrasoft.domain.InsasseEntity;
import ch.ultrasoft.domain.JournalEntity;
import ch.ultrasoft.domain.KonsultationEntity;
import ch.ultrasoft.domain.PersonEntity;
import ch.ultrasoft.domain.StrafeEntity;
import ch.ultrasoft.domain.UrteilEntity;
import ch.ultrasoft.domain.ZelleEntity;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class PersonService extends BaseService<PersonEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public PersonService(){
        super(PersonEntity.class);
    }
    
    @Transactional
    public List<PersonEntity> findAllPersonEntities() {
        
        return entityManager.createQuery("SELECT o FROM Person o ", PersonEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Person o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(PersonEntity person) {

        /* This is called before a Person is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllPersonInsassesAssignments(person);
        
    }

    // Remove all assignments from all insasse a person. Called before delete a person.
    @Transactional
    private void cutAllPersonInsassesAssignments(PersonEntity person) {
        entityManager
                .createQuery("UPDATE Insasse c SET c.person = NULL WHERE c.person = :p")
                .setParameter("p", person).executeUpdate();
    }
    
    @Transactional
    public List<PersonEntity> findAvailablePersonens(KonsultationEntity konsultation) {
        return entityManager.createQuery("SELECT o FROM Person o where o.id not in (select o.id from Person o join o.konsultations p where p = :p)", PersonEntity.class).setParameter("p", konsultation).getResultList();
    }
    
    @Transactional
    public PersonEntity findPersonByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o.insasse.person FROM Aufenthalt o where o.id = :idAufenthalt)", PersonEntity.class).setParameter("idAufenthalt", aufenthalt.getId()).getSingleResult();
    }

    @Transactional
    public List<PersonEntity> findPersonensByKonsultation(KonsultationEntity konsultation) {
        return entityManager.createQuery("SELECT o FROM Person o where o.id in (select o.id from Person o join o.konsultations p where p = :p)", PersonEntity.class).setParameter("p", konsultation).getResultList();
    }

}
