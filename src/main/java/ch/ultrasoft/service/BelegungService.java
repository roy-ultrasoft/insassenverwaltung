package ch.ultrasoft.service;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.BelegungEntity;
import ch.ultrasoft.domain.ZelleEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class BelegungService extends BaseService<BelegungEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public BelegungService(){
        super(BelegungEntity.class);
    }
    
    @Transactional
    public List<BelegungEntity> findAllBelegungEntities() {
        
        return entityManager.createQuery("SELECT o FROM Belegung o ", BelegungEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Belegung o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(BelegungEntity belegung) {

        /* This is called before a Belegung is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<BelegungEntity> findAvailableBelegungs(ZelleEntity zelle) {
        return entityManager.createQuery("SELECT o FROM Belegung o WHERE o.zelle IS NULL", BelegungEntity.class).getResultList();
    }

    @Transactional
    public List<BelegungEntity> findBelegungsByZelle(ZelleEntity zelle) {
        return entityManager.createQuery("SELECT o FROM Belegung o WHERE o.zelle = :zelle", BelegungEntity.class).setParameter("zelle", zelle).getResultList();
    }

    @Transactional
    public List<BelegungEntity> findAvailableBelegungs(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Belegung o WHERE o.aufenthalt IS NULL", BelegungEntity.class).getResultList();
    }

    @Transactional
    public List<BelegungEntity> findBelegungsByAufenthalt(AufenthaltEntity aufenthalt) {
        return entityManager.createQuery("SELECT o FROM Belegung o WHERE o.aufenthalt = :aufenthalt", BelegungEntity.class).setParameter("aufenthalt", aufenthalt).getResultList();
    }

}
