package ch.ultrasoft.web.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.BaseEntity;
import ch.ultrasoft.domain.BelegungEntity;
import ch.ultrasoft.domain.DeliktEntity;
import ch.ultrasoft.domain.EffekteEntity;
import ch.ultrasoft.domain.JournalEntity;
import ch.ultrasoft.domain.KonsultationEntity;
import ch.ultrasoft.domain.StrafeEntity;
import ch.ultrasoft.domain.UrteilEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.BelegungService;
import ch.ultrasoft.service.DeliktService;
import ch.ultrasoft.service.EffekteService;
import ch.ultrasoft.service.JournalService;
import ch.ultrasoft.service.KonsultationService;
import ch.ultrasoft.service.PersonService;
import ch.ultrasoft.service.StrafeService;
import ch.ultrasoft.service.UrteilService;

@Named("aufenthaltViewBean")
@SessionScoped
public class AufenthaltViewBean implements Serializable {

	private static final long serialVersionUID = -6691685309261724790L;

	private Map<String, List<ViewElement>> viewElements = null;

	public class ViewElement {
		private Map<String, String> map = null;
		private BaseEntity entity = null;

		public ViewElement(BaseEntity entity) {
			String title = entity.getClass().getSimpleName();
			map = new HashMap<>();
			for (Field f : entity.getClass().getDeclaredFields()) {
				f.setAccessible(true);
				try {
					map.put(f.getName(), f.get(entity) != null ? f.get(entity).toString() : "");
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}
			this.entity = entity;
		}

		public Map<String, String> getMap() {
			return map;
		}

		public BaseEntity getEntity() {
			return entity;
		}
	}

	@Inject
	private AufenthaltService aufenthaltService;
	@Inject
	private DeliktService deliktService;
	@Inject
	private UrteilService urteilService;
	@Inject
	private KonsultationService konsultationService;
	@Inject
	private PersonService personService;
	@Inject
	private BelegungService belegungService;
	@Inject
	private EffekteService effekteService;
	@Inject
	private StrafeService strafeService;
	@Inject
	private JournalService journalService;

	private AufenthaltEntity selectedAufenthalt;

	public void initViewElements(AufenthaltEntity aufenthalt) {
		viewElements = new TreeMap<>();
		addToViewList(aufenthaltService.find(aufenthalt.getId()));
		addToViewList(personService.findPersonByAufenthalt(aufenthalt));
		for (UrteilEntity u : urteilService.findUrteilsByAufenthalt(aufenthalt)) {
			addToViewList(u);
			for (StrafeEntity s : strafeService.findStrafesByUrteil(u)) {
				addToViewList(s);
			}
		}
		for (DeliktEntity d : deliktService.findDeliktsByAufenthalt(aufenthalt)) {
			addToViewList(d);
		}

		for (EffekteEntity e : effekteService.findEffektesByAufenthalt(aufenthalt)) {
			addToViewList(e);
		}

		for (BelegungEntity b : belegungService.findBelegungsByAufenthalt(aufenthalt)) {
			addToViewList(b);
		}

		for (KonsultationEntity k : konsultationService.findKonsultationsByAufenthalt(aufenthalt)) {
			addToViewList(k);
		}

		for (JournalEntity j : journalService.findJournalsByAufenthalt(aufenthalt)) {
			addToViewList(j);
		}

	}
	
	public void saveSelectedAufenthalt() {
		if (selectedAufenthalt.getId() == null)
			selectedAufenthalt = aufenthaltService.save(selectedAufenthalt);
		else
			selectedAufenthalt = aufenthaltService.update(selectedAufenthalt);
		initViewElements(selectedAufenthalt);
	}

	public void openSelectedAufenthalt(AufenthaltEntity selectedAufenthalt) {
		setSelectedAufenthalt(selectedAufenthalt);
		initViewElements(selectedAufenthalt);
	}
	

	private void addToViewList(BaseEntity aufenthalt) {
		if (!viewElements.containsKey(aufenthalt.getClass().getSimpleName())) {
			viewElements.put(aufenthalt.getClass().getSimpleName(), new ArrayList<>());
		}
		viewElements.get(aufenthalt.getClass().getSimpleName()).add(new ViewElement(aufenthalt));
	}

	public Map<String, List<ViewElement>> getViewElements() {
		if (selectedAufenthalt != null && (viewElements == null || viewElements.isEmpty()))
			initViewElements(selectedAufenthalt);
		return viewElements;
	}

	public AufenthaltEntity getSelectedAufenthalt() {
		return selectedAufenthalt;
	}

	public void setSelectedAufenthalt(AufenthaltEntity selectedAufenthalt) {
		this.selectedAufenthalt = selectedAufenthalt;
	}
}
