package ch.ultrasoft.web;

import ch.ultrasoft.domain.InsasseEntity;
import ch.ultrasoft.domain.PersonEntity;
import ch.ultrasoft.service.InsasseService;
import ch.ultrasoft.service.PersonService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("insasseBean")
@ViewScoped
public class InsasseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(InsasseBean.class.getName());
    
    private List<InsasseEntity> insasseList;

    private InsasseEntity insasse;
    
    @Inject
    private InsasseService insasseService;
    
    @Inject
    private PersonService personService;
    
    private List<PersonEntity> allPersonsList;
    
    public void prepareNewInsasse() {
        reset();
        this.insasse = new InsasseEntity();
        // set any default values now, if you need
        // Example: this.insasse.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (insasse.getId() != null) {
                insasse = insasseService.update(insasse);
                message = "message_successfully_updated";
            } else {
                insasse = insasseService.save(insasse);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        insasseList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            insasseService.delete(insasse);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(InsasseEntity insasse) {
        reset();
        this.insasse = insasse;
    }
    
    public void reset() {
        insasse = null;
        insasseList = null;
        
        allPersonsList = null;
        
    }

    // Get a List of all person
    public List<PersonEntity> getPersons() {
        if (this.allPersonsList == null) {
            this.allPersonsList = personService.findAllPersonEntities();
        }
        return this.allPersonsList;
    }
    
    // Update person of the current insasse
    public void updatePerson(PersonEntity person) {
        this.insasse.setPerson(person);
        // Maybe we just created and assigned a new person. So reset the allPersonList.
        allPersonsList = null;
    }
    
    public InsasseEntity getInsasse() {
        if (this.insasse == null) {
            prepareNewInsasse();
        }
        return this.insasse;
    }
    
    public void setInsasse(InsasseEntity insasse) {
        this.insasse = insasse;
    }
    
    public List<InsasseEntity> getInsasseList() {
        if (insasseList == null) {
            insasseList = insasseService.findAllInsasseEntities();
        }
        return insasseList;
    }

    public void setInsasseList(List<InsasseEntity> insasseList) {
        this.insasseList = insasseList;
    }
    
}
