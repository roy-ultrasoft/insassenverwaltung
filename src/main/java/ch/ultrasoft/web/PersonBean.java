package ch.ultrasoft.web;

import ch.ultrasoft.domain.PersonEntity;
import ch.ultrasoft.service.PersonService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("personBean")
@ViewScoped
public class PersonBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(PersonBean.class.getName());
    
    private List<PersonEntity> personList;

    private PersonEntity person;
    
    @Inject
    private PersonService personService;
    
    public void prepareNewPerson() {
        reset();
        this.person = new PersonEntity();
        // set any default values now, if you need
        // Example: this.person.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (person.getId() != null) {
                person = personService.update(person);
                message = "message_successfully_updated";
            } else {
                person = personService.save(person);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        personList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            personService.delete(person);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(PersonEntity person) {
        reset();
        this.person = person;
    }
    
    public void reset() {
        person = null;
        personList = null;
        
    }

    public PersonEntity getPerson() {
        if (this.person == null) {
            prepareNewPerson();
        }
        return this.person;
    }
    
    public void setPerson(PersonEntity person) {
        this.person = person;
    }
    
    public List<PersonEntity> getPersonList() {
        if (personList == null) {
            personList = personService.findAllPersonEntities();
        }
        return personList;
    }

    public void setPersonList(List<PersonEntity> personList) {
        this.personList = personList;
    }
    
}
