package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.DeliktEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.DeliktService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("deliktBean")
@ViewScoped
public class DeliktBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(DeliktBean.class.getName());
    
    private List<DeliktEntity> deliktList;

    private DeliktEntity delikt;
    
    @Inject
    private DeliktService deliktService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewDelikt() {
        reset();
        this.delikt = new DeliktEntity();
        // set any default values now, if you need
        // Example: this.delikt.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (delikt.getId() != null) {
                delikt = deliktService.update(delikt);
                message = "message_successfully_updated";
            } else {
                delikt = deliktService.save(delikt);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        deliktList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            deliktService.delete(delikt);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(DeliktEntity delikt) {
        reset();
        this.delikt = delikt;
    }
    
    public void reset() {
        delikt = null;
        deliktList = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current delikt
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.delikt.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public DeliktEntity getDelikt() {
        if (this.delikt == null) {
            prepareNewDelikt();
        }
        return this.delikt;
    }
    
    public void setDelikt(DeliktEntity delikt) {
        this.delikt = delikt;
    }
    
    public List<DeliktEntity> getDeliktList() {
        if (deliktList == null) {
            deliktList = deliktService.findAllDeliktEntities();
        }
        return deliktList;
    }

    public void setDeliktList(List<DeliktEntity> deliktList) {
        this.deliktList = deliktList;
    }
    
}
