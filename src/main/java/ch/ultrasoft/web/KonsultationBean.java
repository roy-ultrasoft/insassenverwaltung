package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.KonsultationEntity;
import ch.ultrasoft.domain.PersonEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.KonsultationService;
import ch.ultrasoft.service.PersonService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

@Named("konsultationBean")
@ViewScoped
public class KonsultationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(KonsultationBean.class.getName());
    
    private List<KonsultationEntity> konsultationList;

    private KonsultationEntity konsultation;
    
    @Inject
    private KonsultationService konsultationService;
    
    @Inject
    private PersonService personService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private DualListModel<PersonEntity> personens;
    private List<String> transferedPersonenIDs;
    private List<String> removedPersonenIDs;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewKonsultation() {
        reset();
        this.konsultation = new KonsultationEntity();
        // set any default values now, if you need
        // Example: this.konsultation.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (konsultation.getId() != null) {
                konsultation = konsultationService.update(konsultation);
                message = "message_successfully_updated";
            } else {
                konsultation = konsultationService.save(konsultation);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        konsultationList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            konsultationService.delete(konsultation);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(KonsultationEntity konsultation) {
        reset();
        this.konsultation = konsultation;
    }
    
    public void reset() {
        konsultation = null;
        konsultationList = null;
        
        personens = null;
        transferedPersonenIDs = null;
        removedPersonenIDs = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current konsultation
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.konsultation.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public DualListModel<PersonEntity> getPersonens() {
        return personens;
    }

    public void setPersonens(DualListModel<PersonEntity> persons) {
        this.personens = persons;
    }
    
    public List<PersonEntity> getFullPersonensList() {
        List<PersonEntity> allList = new ArrayList<>();
        allList.addAll(personens.getSource());
        allList.addAll(personens.getTarget());
        return allList;
    }
    
    public void onPersonensDialog(KonsultationEntity konsultation) {
        // Prepare the personen PickList
        this.konsultation = konsultation;
        List<PersonEntity> selectedPersonsFromDB = personService
                .findPersonensByKonsultation(this.konsultation);
        List<PersonEntity> availablePersonsFromDB = personService
                .findAvailablePersonens(this.konsultation);
        this.personens = new DualListModel<>(availablePersonsFromDB, selectedPersonsFromDB);
        
        transferedPersonenIDs = new ArrayList<>();
        removedPersonenIDs = new ArrayList<>();
    }
    
    public void onPersonensPickListTransfer(TransferEvent event) {
        // If a personen is transferred within the PickList, we just transfer it in this
        // bean scope. We do not change anything it the database, yet.
        for (Object item : event.getItems()) {
            String id = ((PersonEntity) item).getId().toString();
            if (event.isAdd()) {
                transferedPersonenIDs.add(id);
                removedPersonenIDs.remove(id);
            } else if (event.isRemove()) {
                removedPersonenIDs.add(id);
                transferedPersonenIDs.remove(id);
            }
        }
        
    }
    
    public void updatePersonen(PersonEntity person) {
        // If a new personen is created, we persist it to the database,
        // but we do not assign it to this konsultation in the database, yet.
        personens.getTarget().add(person);
        transferedPersonenIDs.add(person.getId().toString());
    }
    
    public void onPersonensSubmit() {
        // Now we save the changed of the PickList to the database.
        try {
            
            List<PersonEntity> selectedPersonsFromDB = personService.findPersonensByKonsultation(this.konsultation);
            List<PersonEntity> availablePersonsFromDB = personService.findAvailablePersonens(this.konsultation);

            // Because personens are lazily loaded, we need to fetch them now
            this.konsultation = konsultationService.fetchPersonens(this.konsultation);
            
            for (PersonEntity person : selectedPersonsFromDB) {
                if (removedPersonenIDs.contains(person.getId().toString())) {
                    
                    this.konsultation.getPersonens().remove(person);
                    
                }
            }
    
            for (PersonEntity person : availablePersonsFromDB) {
                if (transferedPersonenIDs.contains(person.getId().toString())) {
                    
                    this.konsultation.getPersonens().add(person);
                    
                }
            }
            
            this.konsultation = konsultationService.update(this.konsultation);
            
            FacesMessage facesMessage = MessageFactory.getMessage("message_changes_saved");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            
            reset();

        } catch (OptimisticLockException e) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_optimistic_locking_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_picklist_save_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }
    
    public KonsultationEntity getKonsultation() {
        if (this.konsultation == null) {
            prepareNewKonsultation();
        }
        return this.konsultation;
    }
    
    public void setKonsultation(KonsultationEntity konsultation) {
        this.konsultation = konsultation;
    }
    
    public List<KonsultationEntity> getKonsultationList() {
        if (konsultationList == null) {
            konsultationList = konsultationService.findAllKonsultationEntities();
        }
        return konsultationList;
    }

    public void setKonsultationList(List<KonsultationEntity> konsultationList) {
        this.konsultationList = konsultationList;
    }
    
}
