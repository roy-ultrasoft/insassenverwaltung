package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.BelegungEntity;
import ch.ultrasoft.domain.ZelleEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.BelegungService;
import ch.ultrasoft.service.ZelleService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("belegungBean")
@ViewScoped
public class BelegungBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(BelegungBean.class.getName());
    
    private List<BelegungEntity> belegungList;

    private BelegungEntity belegung;
    
    @Inject
    private BelegungService belegungService;
    
    @Inject
    private ZelleService zelleService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private List<ZelleEntity> allZellesList;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewBelegung() {
        reset();
        this.belegung = new BelegungEntity();
        // set any default values now, if you need
        // Example: this.belegung.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (belegung.getId() != null) {
                belegung = belegungService.update(belegung);
                message = "message_successfully_updated";
            } else {
                belegung = belegungService.save(belegung);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        belegungList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            belegungService.delete(belegung);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(BelegungEntity belegung) {
        reset();
        this.belegung = belegung;
    }
    
    public void reset() {
        belegung = null;
        belegungList = null;
        
        allZellesList = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all zelle
    public List<ZelleEntity> getZelles() {
        if (this.allZellesList == null) {
            this.allZellesList = zelleService.findAllZelleEntities();
        }
        return this.allZellesList;
    }
    
    // Update zelle of the current belegung
    public void updateZelle(ZelleEntity zelle) {
        this.belegung.setZelle(zelle);
        // Maybe we just created and assigned a new zelle. So reset the allZelleList.
        allZellesList = null;
    }
    
    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current belegung
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.belegung.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public BelegungEntity getBelegung() {
        if (this.belegung == null) {
            prepareNewBelegung();
        }
        return this.belegung;
    }
    
    public void setBelegung(BelegungEntity belegung) {
        this.belegung = belegung;
    }
    
    public List<BelegungEntity> getBelegungList() {
        if (belegungList == null) {
            belegungList = belegungService.findAllBelegungEntities();
        }
        return belegungList;
    }

    public void setBelegungList(List<BelegungEntity> belegungList) {
        this.belegungList = belegungList;
    }
    
}
