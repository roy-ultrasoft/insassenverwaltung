package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.EffekteEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.EffekteService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("effekteBean")
@ViewScoped
public class EffekteBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(EffekteBean.class.getName());
    
    private List<EffekteEntity> effekteList;

    private EffekteEntity effekte;
    
    @Inject
    private EffekteService effekteService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewEffekte() {
        reset();
        this.effekte = new EffekteEntity();
        // set any default values now, if you need
        // Example: this.effekte.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (effekte.getId() != null) {
                effekte = effekteService.update(effekte);
                message = "message_successfully_updated";
            } else {
                effekte = effekteService.save(effekte);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        effekteList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            effekteService.delete(effekte);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(EffekteEntity effekte) {
        reset();
        this.effekte = effekte;
    }
    
    public void reset() {
        effekte = null;
        effekteList = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current effekte
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.effekte.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public EffekteEntity getEffekte() {
        if (this.effekte == null) {
            prepareNewEffekte();
        }
        return this.effekte;
    }
    
    public void setEffekte(EffekteEntity effekte) {
        this.effekte = effekte;
    }
    
    public List<EffekteEntity> getEffekteList() {
        if (effekteList == null) {
            effekteList = effekteService.findAllEffekteEntities();
        }
        return effekteList;
    }

    public void setEffekteList(List<EffekteEntity> effekteList) {
        this.effekteList = effekteList;
    }
    
}
