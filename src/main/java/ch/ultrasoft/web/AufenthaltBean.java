package ch.ultrasoft.web;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.web.util.MessageFactory;

@Named("aufenthaltBean")
@ViewScoped
public class AufenthaltBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(AufenthaltBean.class.getName());
    
    private List<AufenthaltEntity> aufenthaltList;

    private AufenthaltEntity aufenthalt;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    public void prepareNewAufenthalt() {
        reset();
        this.aufenthalt = new AufenthaltEntity();
        // set any default values now, if you need
        // Example: this.aufenthalt.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (aufenthalt.getId() != null) {
                aufenthalt = aufenthaltService.update(aufenthalt);
                message = "message_successfully_updated";
            } else {
                aufenthalt = aufenthaltService.save(aufenthalt);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        aufenthaltList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            aufenthaltService.delete(aufenthalt);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(AufenthaltEntity aufenthalt) {
        reset();
        this.aufenthalt = aufenthalt;
    }
    
    public void prepare() {
		aufenthaltService.prepare();
		reset();
	}
    
    public void reset() {
        aufenthalt = null;
        aufenthaltList = null;
        
    }

    public AufenthaltEntity getAufenthalt() {
        if (this.aufenthalt == null) {
            prepareNewAufenthalt();
        }
        return this.aufenthalt;
    }
    
    public void setAufenthalt(AufenthaltEntity aufenthalt) {
        this.aufenthalt = aufenthalt;
    }
    
    public List<AufenthaltEntity> getAufenthaltList() {
        if (aufenthaltList == null || aufenthaltList.isEmpty()) {
            aufenthaltList = aufenthaltService.findAllAufenthaltEntities();
        }
        return aufenthaltList;
    }

    public void setAufenthaltList(List<AufenthaltEntity> aufenthaltList) {
        this.aufenthaltList = aufenthaltList;
    }
    
}
