package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.JournalEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.JournalService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("journalBean")
@ViewScoped
public class JournalBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(JournalBean.class.getName());
    
    private List<JournalEntity> journalList;

    private JournalEntity journal;
    
    @Inject
    private JournalService journalService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewJournal() {
        reset();
        this.journal = new JournalEntity();
        // set any default values now, if you need
        // Example: this.journal.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (journal.getId() != null) {
                journal = journalService.update(journal);
                message = "message_successfully_updated";
            } else {
                journal = journalService.save(journal);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        journalList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            journalService.delete(journal);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(JournalEntity journal) {
        reset();
        this.journal = journal;
    }
    
    public void reset() {
        journal = null;
        journalList = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current journal
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.journal.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public JournalEntity getJournal() {
        if (this.journal == null) {
            prepareNewJournal();
        }
        return this.journal;
    }
    
    public void setJournal(JournalEntity journal) {
        this.journal = journal;
    }
    
    public List<JournalEntity> getJournalList() {
        if (journalList == null) {
            journalList = journalService.findAllJournalEntities();
        }
        return journalList;
    }

    public void setJournalList(List<JournalEntity> journalList) {
        this.journalList = journalList;
    }
    
}
