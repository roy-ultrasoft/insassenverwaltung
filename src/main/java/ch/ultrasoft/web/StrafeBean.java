package ch.ultrasoft.web;

import ch.ultrasoft.domain.StrafeEntity;
import ch.ultrasoft.domain.UrteilEntity;
import ch.ultrasoft.service.StrafeService;
import ch.ultrasoft.service.UrteilService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("strafeBean")
@ViewScoped
public class StrafeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(StrafeBean.class.getName());
    
    private List<StrafeEntity> strafeList;

    private StrafeEntity strafe;
    
    @Inject
    private StrafeService strafeService;
    
    @Inject
    private UrteilService urteilService;
    
    private List<UrteilEntity> allUrteilsList;
    
    public void prepareNewStrafe() {
        reset();
        this.strafe = new StrafeEntity();
        // set any default values now, if you need
        // Example: this.strafe.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (strafe.getId() != null) {
                strafe = strafeService.update(strafe);
                message = "message_successfully_updated";
            } else {
                strafe = strafeService.save(strafe);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        strafeList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            strafeService.delete(strafe);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(StrafeEntity strafe) {
        reset();
        this.strafe = strafe;
    }
    
    public void reset() {
        strafe = null;
        strafeList = null;
        
        allUrteilsList = null;
        
    }

    // Get a List of all urteil
    public List<UrteilEntity> getUrteils() {
        if (this.allUrteilsList == null) {
            this.allUrteilsList = urteilService.findAllUrteilEntities();
        }
        return this.allUrteilsList;
    }
    
    // Update urteil of the current strafe
    public void updateUrteil(UrteilEntity urteil) {
        this.strafe.setUrteil(urteil);
        // Maybe we just created and assigned a new urteil. So reset the allUrteilList.
        allUrteilsList = null;
    }
    
    public StrafeEntity getStrafe() {
        if (this.strafe == null) {
            prepareNewStrafe();
        }
        return this.strafe;
    }
    
    public void setStrafe(StrafeEntity strafe) {
        this.strafe = strafe;
    }
    
    public List<StrafeEntity> getStrafeList() {
        if (strafeList == null) {
            strafeList = strafeService.findAllStrafeEntities();
        }
        return strafeList;
    }

    public void setStrafeList(List<StrafeEntity> strafeList) {
        this.strafeList = strafeList;
    }
    
}
