package ch.ultrasoft.web;

import ch.ultrasoft.domain.AufenthaltEntity;
import ch.ultrasoft.domain.UrteilEntity;
import ch.ultrasoft.service.AufenthaltService;
import ch.ultrasoft.service.UrteilService;
import ch.ultrasoft.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("urteilBean")
@ViewScoped
public class UrteilBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(UrteilBean.class.getName());
    
    private List<UrteilEntity> urteilList;

    private UrteilEntity urteil;
    
    @Inject
    private UrteilService urteilService;
    
    @Inject
    private AufenthaltService aufenthaltService;
    
    private List<AufenthaltEntity> allAufenthaltsList;
    
    public void prepareNewUrteil() {
        reset();
        this.urteil = new UrteilEntity();
        // set any default values now, if you need
        // Example: this.urteil.setAnything("test");
    }

    public String persist() {

        String message;
        
        try {
            
            if (urteil.getId() != null) {
                urteil = urteilService.update(urteil);
                message = "message_successfully_updated";
            } else {
                urteil = urteilService.save(urteil);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        urteilList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        String message;
        
        try {
            urteilService.delete(urteil);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(UrteilEntity urteil) {
        reset();
        this.urteil = urteil;
    }
    
    public void reset() {
        urteil = null;
        urteilList = null;
        
        allAufenthaltsList = null;
        
    }

    // Get a List of all aufenthalt
    public List<AufenthaltEntity> getAufenthalts() {
        if (this.allAufenthaltsList == null) {
            this.allAufenthaltsList = aufenthaltService.findAllAufenthaltEntities();
        }
        return this.allAufenthaltsList;
    }
    
    // Update aufenthalt of the current urteil
    public void updateAufenthalt(AufenthaltEntity aufenthalt) {
        this.urteil.setAufenthalt(aufenthalt);
        // Maybe we just created and assigned a new aufenthalt. So reset the allAufenthaltList.
        allAufenthaltsList = null;
    }
    
    public UrteilEntity getUrteil() {
        if (this.urteil == null) {
            prepareNewUrteil();
        }
        return this.urteil;
    }
    
    public void setUrteil(UrteilEntity urteil) {
        this.urteil = urteil;
    }
    
    public List<UrteilEntity> getUrteilList() {
        if (urteilList == null) {
            urteilList = urteilService.findAllUrteilEntities();
        }
        return urteilList;
    }

    public void setUrteilList(List<UrteilEntity> urteilList) {
        this.urteilList = urteilList;
    }
    
}
