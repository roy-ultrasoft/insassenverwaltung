package ch.ultrasoft.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name="Konsultation")
@Table(name="\"KONSULTATION\"")
public class KonsultationEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name="\"von\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date von;

    @Column(name="\"bis\"")
    @Temporal(TemporalType.DATE)
    private Date bis;

    @Size(max = 1024)
    @Column(length = 1024, name="\"text\"")
    private String text;

    @ManyToMany(fetch=FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinTable(name="KONSULTATION_PERSONENS",
              joinColumns={@JoinColumn(name="KONSULTATION_ID", referencedColumnName="ID")},
              inverseJoinColumns={@JoinColumn(name="PERSONEN_ID", referencedColumnName="ID")})
    private List<PersonEntity> personens;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AUFENTHALT_ID", referencedColumnName = "ID")
    private AufenthaltEntity aufenthalt;

    public Date getVon() {
        return this.von;
    }

    public void setVon(Date von) {
        this.von = von;
    }

    public Date getBis() {
        return this.bis;
    }

    public void setBis(Date bis) {
        this.bis = bis;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<PersonEntity> getPersonens() {
        return personens;
    }

    public void setPersonens(List<PersonEntity> persons) {
        this.personens = persons;
    }

    public AufenthaltEntity getAufenthalt() {
        return this.aufenthalt;
    }

    public void setAufenthalt(AufenthaltEntity aufenthalt) {
        this.aufenthalt = aufenthalt;
    }

}
