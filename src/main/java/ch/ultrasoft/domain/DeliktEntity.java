package ch.ultrasoft.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity(name="Delikt")
@Table(name="\"DELIKT\"")
public class DeliktEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 1024)
    @Column(length = 1024, name="\"text\"")
    private String text;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AUFENTHALT_ID", referencedColumnName = "ID")
    private AufenthaltEntity aufenthalt;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AufenthaltEntity getAufenthalt() {
        return this.aufenthalt;
    }

    public void setAufenthalt(AufenthaltEntity aufenthalt) {
        this.aufenthalt = aufenthalt;
    }

}
