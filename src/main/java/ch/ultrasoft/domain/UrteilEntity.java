package ch.ultrasoft.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name="Urteil")
@Table(name="\"URTEIL\"")
public class UrteilEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 100)
    @Column(length = 100, name="\"text\"")
    private String text;

    @Column(name="\"datum\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date datum;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AUFENTHALT_ID", referencedColumnName = "ID")
    private AufenthaltEntity aufenthalt;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatum() {
        return this.datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public AufenthaltEntity getAufenthalt() {
        return this.aufenthalt;
    }

    public void setAufenthalt(AufenthaltEntity aufenthalt) {
        this.aufenthalt = aufenthalt;
    }

}
