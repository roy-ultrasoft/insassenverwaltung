package ch.ultrasoft.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name="Insasse")
@Table(name="\"INSASSE\"")
public class InsasseEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne(optional=true)
    @JoinColumn(name = "PERSON_ID", referencedColumnName = "ID")
    private PersonEntity person;

    public PersonEntity getPerson() {
        return this.person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

}
