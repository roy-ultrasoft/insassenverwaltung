package ch.ultrasoft.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity(name="Belegung")
@Table(name="\"BELEGUNG\"")
public class BelegungEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name="\"von\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date von;

    @Column(name="\"bis\"")
    @Temporal(TemporalType.DATE)
    private Date bis;

    @ManyToOne(optional=true)
    @JoinColumn(name = "ZELLE_ID", referencedColumnName = "ID")
    private ZelleEntity zelle;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AUFENTHALT_ID", referencedColumnName = "ID")
    private AufenthaltEntity aufenthalt;

    public Date getVon() {
        return this.von;
    }

    public void setVon(Date von) {
        this.von = von;
    }

    public Date getBis() {
        return this.bis;
    }

    public void setBis(Date bis) {
        this.bis = bis;
    }

    public ZelleEntity getZelle() {
        return this.zelle;
    }

    public void setZelle(ZelleEntity zelle) {
        this.zelle = zelle;
    }

    public AufenthaltEntity getAufenthalt() {
        return this.aufenthalt;
    }

    public void setAufenthalt(AufenthaltEntity aufenthalt) {
        this.aufenthalt = aufenthalt;
    }

}
