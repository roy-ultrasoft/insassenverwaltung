package ch.ultrasoft.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name="Person")
@Table(name="\"PERSON\"")
public class PersonEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(length = 50, name="\"name\"")
    @NotNull
    private String name;

    @Size(max = 50)
    @Column(length = 50, name="\"vorname\"")
    @NotNull
    private String vorname;

    @Column(name="\"geburtsdatum\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date geburtsdatum;

    public void setKonsultations(List<KonsultationEntity> konsultations) {
        this.konsultations = konsultations;
    }

    public List<KonsultationEntity> getKonsultations() {
        return konsultations;
    }

    @ManyToMany(mappedBy="personens", fetch=FetchType.LAZY, cascade = CascadeType.DETACH)
    private List<KonsultationEntity> konsultations;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return this.vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public Date getGeburtsdatum() {
        return this.geburtsdatum;
    }

    public void setGeburtsdatum(Date geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

}
