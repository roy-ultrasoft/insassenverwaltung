package ch.ultrasoft.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity(name="Aufenthalt")
@Table(name="\"AUFENTHALT\"")
public class AufenthaltEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name="\"von\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date von;

    @Column(name="\"bis\"")
    @Temporal(TemporalType.DATE)
    private Date bis;
    
    @ManyToOne(optional=true)
    @JoinColumn(name = "INSASSE_ID", referencedColumnName = "ID")
    private InsasseEntity insasse;

    public Date getVon() {
        return this.von;
    }

    public void setVon(Date von) {
        this.von = von;
    }

    public Date getBis() {
        return this.bis;
    }

    public void setBis(Date bis) {
        this.bis = bis;
    }

	public InsasseEntity getInsasse() {
		return insasse;
	}

	public void setInsasse(InsasseEntity insasse) {
		this.insasse = insasse;
	}
}
