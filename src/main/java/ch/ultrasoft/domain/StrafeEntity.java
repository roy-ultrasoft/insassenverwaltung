package ch.ultrasoft.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name="Strafe")
@Table(name="\"STRAFE\"")
public class StrafeEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(length = 50, name="\"text\"")
    @NotNull
    private String text;

    @ManyToOne(optional=true)
    @JoinColumn(name = "URTEIL_ID", referencedColumnName = "ID")
    private UrteilEntity urteil;

    @Column(name="\"tage\"")
    @Digits(integer = 4, fraction = 0)
    private Integer tage;

    @Column(name="\"monate\"")
    @Digits(integer = 4, fraction = 0)
    private Integer monate;

    @Column(name="\"jahre\"")
    @Digits(integer = 4, fraction = 0)
    private Integer jahre;

    @Column(name="\"addieren\"")
    private Boolean addieren;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UrteilEntity getUrteil() {
        return this.urteil;
    }

    public void setUrteil(UrteilEntity urteil) {
        this.urteil = urteil;
    }

    public Integer getTage() {
        return this.tage;
    }

    public void setTage(Integer tage) {
        this.tage = tage;
    }

    public Integer getMonate() {
        return this.monate;
    }

    public void setMonate(Integer monate) {
        this.monate = monate;
    }

    public Integer getJahre() {
        return this.jahre;
    }

    public void setJahre(Integer jahre) {
        this.jahre = jahre;
    }

    public Boolean getAddieren() {
        return this.addieren;
    }

    public void setAddieren(Boolean addieren) {
        this.addieren = addieren;
    }

}
